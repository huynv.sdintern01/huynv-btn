<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Coin;
use App\Models\Cvs;
use App\Models\HandleCoin;
use App\Models\JobCategory;
use App\Models\UserModel;
use App\Models\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RecruitController extends Controller
{
    private $job;
    private $hdcoin;
    private $coins;
    private $view;
    private $major;
    private $userModel;
    public function __construct()
    {
        $this->job = new JobCategory();
        $this->hdcoin = new HandleCoin();     
        $this->coins = new Coin();
        $this->view = new View();
        $this->userModel = new UserModel();
        $this->major = UserModel::all()->unique('major');      
    }
    public function listJob()
    { 
        $data = UserModel::with('job_categorie')->where('id', Auth::id())->first();
        return view('recruit.list_job', [
            'data' => $data, 
            'coin' => Auth::user()->coin, 
            'major' => $this->major, 
        ]);
    }
    public function addJob()
    {
        return view('recruit.add_job', [
            'coin' => Auth::user()->coin, 
            'major' => $this->major, 
        ]);
    }
    public function startAddJob(Request $request)
    {
        $this->job->add($request);
        return redirect()->route('list_job');
    }
    public function updateJob(Request $request)
    {
        $data = $this->job->getJob($request->id);
        return view('recruit.update_job', [
            'data' => $data, 
            'coin' => Auth::user()->coin, 
            'major' => $this->major, 
        ]);
    }
    public function startUpdateJob(Request $request)
    {
        $this->job->updateJob($request);
        return redirect()->route('list_job');
    }
    public function deleteJob(Request $request)
    {
        $this->job->delJob($request->id);
        return redirect()->route('list_job');
    }
    public function payCoin ()
    {
        return view('recruit.pay_coin', [
            'coin' => Auth::user()->coin, 
            'major' => $this->major, 
        ]);
    }
    public function payIn(Request $request)
    {
        $this->hdcoin->addHandleCoin($request);
        return redirect()->route('pay_coin');
    }
    public function listUserNormal(Request $request)
    {
        $data = UserModel::where('major', $request->major)->where('level', 3)->get();
        return view('recruit.list_normal_user', [
            'data' => $data, 
            'coin' => Auth::user()->coin, 
            'major' => $this->major, 
        ]);
    }
    public function viewUser(Request $request)
    {
        $path = Cvs::where('id_user', $request->id)->where('status', 'active')->get();
        $this->coins->coinView($request);
        $this->view->updateView($request);
        return redirect()->to(asset($path[0]->file_path));
    }
}