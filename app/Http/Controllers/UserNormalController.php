<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Coin;
use App\Models\Cvs;
use App\Models\JobCategory;
use App\Models\UserModel;
use App\Models\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsernormalController extends Controller
{
    private $userModel;
    private $job;
    private $coin;
    private $cv;
    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->job = new JobCategory();
        $this->coin = new Coin();
        $this->cv = new Cvs();
    }
    public function listJobType()
    {
        $data = $this->job->getTypeJob();
        return view('normal.list_job', compact('data'));
    }
    public function listJobByCareer(Request $request)
    {
        $data2 = $this->job->getJobByType($request->career);
        $data = $this->job->getTypeJob();
        return view('normal.list_job', compact(['data', 'data2']));
    }
    public function upload()
    {
        $data = $this->job->getTypeJob();
        return view('normal.upload', compact('data'));
    }
    public function startUpload(Request $request)
    {
        $this->cv->uploadCV($request);
        return redirect()->route('user_normal_list_job');
    }
    public function viewCV()
    {
        $id_view = Auth::user()->view->pluck('id_recruit')->toArray();
        $data_view = UserModel::whereIn('id', $id_view)->get();
        return view('normal.list_user_view', ['data_view' => $data_view]);
    }
    // public function updateProfile(UserRequest $request)
    // {
    //     $this->userModel->updateUser($request);
    //     return redirect()->route('log_out');
    // }
}