<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cvs extends Model
{
    use HasFactory;
    protected $table = "cvs";
    protected $fillable = ['id_user', 'file_path', 'updated_at'];
    public function uploadCV($request)
    {
        $cvs = new Cvs();
        $cvs->id_user = $request->id;
        $cvs->file_path = $request->id."/".$request->cv->getClientOriginalName();
        $cvs->updated_at = $request->date;
        $cvs->save();
        $request->cv->move($request->id,$request->cv->getClientOriginalName());
    }
}