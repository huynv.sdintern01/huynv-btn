<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
    use HasFactory;
    protected $table = "job_categories";
    public function user()
    {
        return $this->belongsTo(UserModel::class, 'id_recruit', 'id');
    }
    public function allJob()
    {
        return JobCategory::all();
    }
    public function delJob($id)
    {
        $job = JobCategory::find($id);
        $job->delete();
    }
    public function add($request)
    {
        $job = new JobCategory();
        $job->id_recruit = $request->id_recruit;
        $job->name_job = $request->name_job;
        $job->career = $request->career;
        $job->save();
    }
    public function getJob($id)
    {
        return JobCategory::find($id);
    }
    public function updateJob($request)
    {
        $job = JobCategory::find($request->id);
        $job->id_recruit = $request->id_recruit;
        $job->name_job = $request->name_job;
        $job->career = $request->career;
        $job->save();
        return $request->url;
    }
    public function getTypeJob()
    {
        return JobCategory::all()->unique("career");
    }
    public function getJobByType($career)
    {
        return JobCategory::where('career', $career)->get();
    }
}