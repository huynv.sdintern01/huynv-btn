
@extends('layout.main')

@section('tittle')
    <title>Add New Job</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_recruit')}}</a>
@endsection

@section('coin')
    {{!empty($coin->coin_number)? $coin->coin_number : 0}} coins
@endsection

@section('menu')
    @include('blocks/menu_recruit')
@endsection

@section('content')
    <form action="{{route('start_add_job')}}" method="post">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Name Job</label>
                <input type="text" class="form-control"
                    name="name_job" id="exampleInputEmail1"
                    placeholder="User Name">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Career</label>
                <input type="text" class="form-control"
                    name="career" id="exampleInputPassword1"
                    placeholder="Name">
                <input type="hidden" name="id_recruit" value="{{Auth::id()}}">
            </div>
        </div>


        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Add New
                Job</button>
        </div>
    </form>
@endsection