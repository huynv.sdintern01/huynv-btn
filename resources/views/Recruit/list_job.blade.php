@extends('layout.main')

@section('tittle')
    <title>List Job</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_recruit')}}</a>
@endsection

@section('coin')
    {{!empty($coin->coin_number)? $coin->coin_number : 0}} coins
@endsection

@section('menu')
    @include('blocks/menu_recruit')
@endsection
@section('content')
<table id="example2"
class="table table-bordered table-hover">
<thead>
    <tr>
        <th>ID</th>
        <th>Name Job</th>
        <th>Career</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    <@foreach( $data->job_categorie as $value)
    <tr>
        <th scope="row">{{$value["id"]}}</th>
        <td>{{$value["name_job"] }}</td>
        <td>{{$value["career"] }}</td>
        <td>
            <form action="{{route('update_job', ['id' => $value['id']])}}" method="post">
                @csrf
                @method('get')
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
            <form action="{{route('delete_job', ['id' => $value['id']])}}" method="post">
                @csrf
                @method('delete')
                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</tbody>
</table>
@endsection