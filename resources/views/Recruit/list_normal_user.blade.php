@extends('layout.main')

@section('tittle')
    <title>List User Normal</title>
@endsection

@section('user_name')
    <a  href="" class="d-block">{{Session::get('user_name_recruit')}}</a>
@endsection

@section('coin')
    {{!empty($coin->coin_number)? $coin->coin_number : 0}} coins
@endsection

@section('menu')
    @include('blocks/menu_recruit')
@endsection

@section('content')
<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>User Name</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Age</th>
            <th>Major</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $data as $value)
        <tr>
            <th scope="row">{{ $value["id"]; }}</th>
            <td>{{ $value["user_name"] }}</td>
            <td>{{ $value["name"]; }}</td>
            <td>{{ $value["email"]; }}</td>
            <td>{{ $value["phone_number"]; }}</td>
            <td>{{ $value["age"]; }}</td>
            <td>{{ $value["major"]; }}</td>
            <td>
                <a class="btn btn-primary" href="{{route('view_user', ['id' => $value['id'], 'id_recruit' => Auth::id()])}}" target="_back">View</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection