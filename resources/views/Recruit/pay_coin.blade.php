@extends('layout.main')

@section('tittle')
    <title>Pay coin</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_recruit')}}</a>
@endsection

@section('coin')
    {{!empty($coin->coin_number)? $coin->coin_number : 0}} coins
@endsection

@section('menu')
    @include('blocks/menu_recruit')
@endsection

@section('content')
    <form action="{{route('pay_in')}}" method="post">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Choose a
                    denomination</label>
                <select class="form-control" name="cost" id="">
                    <option value="50000">50.000 VND</option>
                    <option value="100000">100.000 VND</option>
                    <option value="200000">200.000 VND</option>
                    <option value="500000">500.000 VND</option>
                </select>
                <input type="hidden" name="id_recruit"
                    value="{{Auth::id()}}">
                <input type="hidden" name="status"
                    value="Pending">
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Pay
                In</button>
        </div>
    </form>
@endsection