<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('fonts/icomoon/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('fonts/icomoon/style.css')}}">
    <title>Sig In</title>
</head>

<body>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-md-2">
                    <img src="{{asset('images/undraw_file_sync_ot38.svg')}}" alt="Image" class="img-fluid">
                </div>
                <div class="col-md-6 contents">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="mb-4">
                                <h3>Welcome to <strong>My Web</strong></h3>
                                <p style="color: red;" class="mb-4">To Creating account, you have to:</p>
                                <p style="color: red;">- Write all your information</p>
                                <p style="color: red;">- Phone number and age must be numbers</p>
                                <p style="color: red;">- Definitely Password equal Confirm Password</p>
                            </div>
                            <form action="{{route('startSigin')}}" method="post">
                                @csrf
                                <div class="form-group first">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" id="username" name="userName" value="{{old('userName')}}">
                                    @error('userName')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="pass" value="{{old('pass')}}">
                                    @error('pass')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Confirm Password</label>
                                    <input type="password" class="form-control" id="password" name="cfpass" value="{{old('cfpass')}}">
                                    @error('cfpass')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                    @error('name')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Email</label>
                                    <input type="text" class="form-control" id="" name="email" value="{{old('email')}}">
                                    @error('email')
                                         <span style="color: red;">{{$message}}</span>
                                     @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Phone Number</label>
                                    <input type="text" class="form-control" id="" name="phoneNum" value="{{old('phoneNum')}}">
                                    @error('phoneNum')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Age</label>
                                    <input type="text" class="form-control" id="" name="age" value="{{old('age')}}">
                                    @error('age')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Major</label>
                                    <input type="text" class="form-control" id="" name="major" value="{{old('major')}}">
                                    @error('major')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <span>User Type</span><br>
                                    <select name="level" id="">
                                        <option value="2">User Recruiter</option>
                                        <option value="3">User Normal</option>
                                    </select>
                                </div>

                                <input type="submit" value="Sig In" class="btn text-white btn-block btn-primary">
                                <div class="d-flex mb-5 align-items-center">
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('public/js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
</body>

</html>