@extends('layout.main')

@section('tittle')
    <title>List User Admin</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_admin')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_admin')
@endsection

@section('content')
<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>User Name</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Age</th>
            <th>Major</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $data as $value)
        <tr>
            <th scope="row">{{ $value["id"]; }}</th>
            <td>{{ $value["user_name"] }}</td>
            <td>{{ $value["name"]; }}</td>
            <td>{{ $value["email"]; }}</td>
            <td>{{ $value["phone_number"]; }}</td>
            <td>{{ $value["age"]; }}</td>
            <td>{{ $value["major"]; }}</td>
            <td>
                <form action="{{route('update_user', ['id' => $value['id']])}}" method="post">
                    @csrf
                    @method('get')
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
                <form action="{{route('delete_user', ['id' => $value['id']])}}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection