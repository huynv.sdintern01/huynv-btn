@extends('layout.main')

@section('tittle')
    <title>Update User</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_admin')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_admin')
@endsection

@section('content')
    <form action="{{route('start_add_admin')}}" method="post">
        @csrf
        <div class="form-group first">
            <input type="hidden" name="url" value="{{$url}}">
            <input type="hidden" name="id" value="{{$data['id']}}">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="userName" value="{{$data['user_name']}}">
            @error('userName')
                <span style="color: red;">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group last mb-4">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="pass" value="{{$data['password']}}">
            @error('pass')
                <span style="color: red;">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group last mb-4">
            <label for="password">Confirm Password</label>
            <input type="password" class="form-control" id="password" name="cfPass" value="{{$data['password']}}">
            @error('pass')
                <span style="color: red;">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group last mb-4">
            <label for="password">Name</label>
            <input type="text" class="form-control" name="name" value="{{$data['name']}}">
            @error('name')
                <span style="color: red;">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group last mb-4">
            <label for="password">Email</label>
            <input type="text" class="form-control" id="" name="email" value="{{$data['email']}}">
            @error('email')
                <span style="color: red;">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group last mb-4">
            <label for="password">Phone Number</label>
            <input type="text" class="form-control" id="" name="phoneNum" value="{{$data['phone_number']}}">
            @error('phoneNum')
                <span style="color: red;">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group last mb-4">
            <label for="password">Age</label>
            <input type="text" class="form-control" id="" name="age" value="{{$data['age']}}">
            @error('age')
                <span style="color: red;">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group last mb-4">
            <label for="password">Major</label>
            <input type="text" class="form-control" id="" name="major" value="{{$data['major']}}">
            @error('major')
                <span style="color: red;">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group last mb-4">
            <span>User Type</span><br>
            <select name="level" id="">
                <option value="1">User Admin</option>
                <option value="2">User Recruiter</option>
                <option value="3">User Normal</option>
            </select>
        </div>
        <input type="submit" value="Update" class="btn text-white btn-block btn-primary">
        <div class="d-flex mb-5 align-items-center">
        </div>
    </form>
@endsection