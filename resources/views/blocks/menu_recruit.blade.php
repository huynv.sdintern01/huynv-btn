<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
    data-accordion="false">
    <li class="nav-item">
        <a href="{{route('list_job')}}" class="nav-link {{url()->current() == 'http://btn.test/recruit/list_job' ? 'active' : ''}}">
            <i class="nav-icon fas fa-copy"></i>
            List Job Categories
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('add_job')}}" class="nav-link {{url()->current() == 'http://btn.test/recruit/add_job' ? 'active' : ''}}">
            <i class="nav-icon fas fa-copy"></i>
            Add New Job
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('pay_coin')}}" class="nav-link {{url()->current() == 'http://btn.test/recruit/pay_coin' ? 'active' : ''}}">
            <i class="nav-icon fas fa-copy"></i>
            Pay Coin
        </a>
    </li>
    <li class="nav-item">
        
        <a href="{{route('list_career')}}" class="nav-link">
            <i class="nav-icon far fa-envelope"></i>
            <p>
            List Career
            <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @if (isset($major))
                @foreach($major as $value)
                <li class="nav-item">
                    <a href="{{route('list_user', ['major' => $value['major']])}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>{{$value['major']}}</p>
                    </a>
                </li>
                @endforeach
            @endif
        </ul>
    </li>
</ul>