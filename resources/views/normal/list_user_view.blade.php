@extends('layout.main')

@section('tittle')
    <title>List User View</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_normal')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_normal')
@endsection

@section('content')
<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Age</th>
            <th>Major</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $data_view as $value)
        <tr>
            <th scope="row">{{ $value["id"]; }}</th>
            <td>{{ $value["name"]; }}</td>
            <td>{{ $value["email"]; }}</td>
            <td>{{ $value["phone_number"]; }}</td>
            <td>{{ $value["age"]; }}</td>
            <td>{{ $value["major"]; }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection