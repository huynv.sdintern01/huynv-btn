@extends('layout.main')

@section('tittle')
    <title>List Job</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_normal')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_normal')
@endsection

@section('content')
    @include('blocks.profile')                                   
@endsection
